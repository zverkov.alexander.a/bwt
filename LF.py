"""
http://rosalind.info/problems/ba9k/

INPUT:
L = T$GACCA
3

OUTPUT:
1

Что можно оптимизировать? Например не пересчитывать С, occ каждый раз
"""


def C(s: str, x: str) -> int:
    """
    C(x) – возвращает кол-во символов в строке s, лексиграфически меньших, чем x.
    :param s:
    :param x:
    :return:
    """
    n = 0
    for i in range(0, len(s)):
        if s[i] < x:
            n = n + 1
    return n


def occ(L: str, x: str, t: int) -> int:
    """
    Occ(L, x, t) – возвращает кол-во символов x в префиксе строки L длиной t (т.е.
    в подстроке L[1..t]).
    :param L:
    :param x:
    :param t:
    :return:
    """
    n = 0
    for i in range(0, t):
        if L[i] == x:
            n = n + 1
    return n


def last_to_first(L: str, i: int) -> int:
    """
    функция «last-to-first помогает определить i-го символа в последнем столбце  зная его ранг (rank(x)),
     мы легко  позицию в первом столбце.
    :param s:
    :param i:
    :return:
    """
    k = C(L, L[i]) + occ(L, L[i], i)
    return k


def solution(input_file_path:str, output_file_path:str):
    with open(input_file_path, 'r') as file:
        L = file.readline().strip()
        i = int(file.readline().strip())

    output = last_to_first(L, i)

    with open(output_file_path, 'w') as file:
        file.write(str(output))


def last_to_first_test(input_str:str='T$GACCA', i:int=3, answer:int=1):
    assert last_to_first(input_str, i) == answer


if __name__ == "__main__":
    last_to_first_test()
    solution(input_file_path='./input_data/rosalind_ba9k.txt',
             output_file_path='./input_data/rosalind_ba9k_output.txt')
