"""
решение http://rosalind.info/problems/ba9i/
"""

def shift(input_str: str) -> str:
    """
    циклический сдвиг строки
    TATATAGA$
    ATATAGA$T
    """
    return input_str[1:] + input_str[0]


def bwt(input_str: str) -> str:
    """
    bwt  преобразование
    :param input_str:
    :return:
    """
    bwt_list = [input_str]
    for i in range(len(input_str) - 1):
        bwt_list.append(shift(bwt_list[-1]))
    bwt_list = sorted(bwt_list)
    output_str = ''.join([i[-1] for i in bwt_list])
    return output_str


def shift_test(input_str: str = "TATATAGA$", output_str: str = "ATATAGA$T"):
    assert output_str == shift(input_str)


def bwt_test(input_str: str = "GCGTGCCTGGTCA$", output_str: str = "ACTGGCT$TGCGGC"):
    assert output_str == bwt(input_str)


def read_bwt_input_file(path_to_file: str = 'rosalind_ba9i.txt') -> str:
    with open(path_to_file) as file:
        input_str = file.readline().strip()
    return input_str


def solution(path_to_file: str = 'rosalind_ba9i.txt',
             path_to_output_file: str = 'rosalind_ba9i_output.txt'
             ):
    input_str = read_bwt_input_file(path_to_file)
    output_str = bwt(input_str)
    with open(path_to_output_file, 'w') as file:
        file.write(output_str)


if __name__ == "__main__":
    # shift_test()
    # bwt_test()
    solution()

