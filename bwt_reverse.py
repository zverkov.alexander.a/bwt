"""
Reconstruct a String from its Burrows-Wheeler Transform
 http://rosalind.info/problems/ba9j/

 input: TTCCTAACG$A
 output: TACATCACGT$

L  -> sort  -> +L   -> sort ...

"""


def reverse_bwt(input_str: str) -> str:
    print('input_str: ', input_str)
    input_str_sorted = sorted(input_str)

    # fixme zip()
    for j in range(len(input_str) - 1):
        for i in range(len(input_str)):
            input_str_sorted[i] = input_str[i] + input_str_sorted[i]
        input_str_sorted = sorted(input_str_sorted)

    for i in input_str_sorted:
        if i[-1] == '$':
            input_str_sorted = i

    return input_str_sorted


def reverse_bwt_test(input_str: str, output_str: str):
    assert output_str == reverse_bwt(input_str)



def read_bwt_reverse_input_file(path_to_file: str = './input_data/rosalind_ba9j.txt') -> str:
    with open(path_to_file) as file:
        input_str = file.readline().strip()
    return input_str


def solution(path_to_file: str = './input_data/rosalind_ba9j.txt',
             path_to_output_file: str = './input_data/rosalind_ba9j_output.txt'
             ):
    input_str = read_bwt_reverse_input_file(path_to_file)
    output_str = reverse_bwt(input_str)
    with open(path_to_output_file, 'w') as file:
        file.write(output_str)

if __name__ == "__main__":
    # reverse_bwt_test(input_str='TTCCTAACG$A', output_str='TACATCACGT$')

    # result = reverse_bwt(input_str='TTCCTAACG$A')
    # reverse_bwt_test(input_str='TTCCTAACG$A', output_str='TACATCACGT$')
    # print(result)

    solution()
