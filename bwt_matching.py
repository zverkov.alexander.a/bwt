"""
Implementing BWMatching
http://rosalind.info/problems/ba9l/
"""

from typing import List
from LF import last_to_first


def get_first_position(symbol: str, s: str, top: int, bottom: int) -> int:
    for i in range(top, bottom + 1):
        if symbol == s[i]:
            return i


def get_last_position(symbol: str, s: str, top: int, bottom: int) -> int:
    for i in range(bottom, (top - 1), -1):
        if symbol == s[i]:
            return i


def bwt_matching(last_column: str, pattern: str) -> int:
    top = 0
    bottom = len(last_column) - 1
    while top <= bottom:
        if pattern != '':
            symbol = pattern[-1]
            if len(pattern) > 1:
                pattern = pattern[:-1]
            else:
                pattern = ''

            if symbol in last_column[top:(bottom + 1)]:
                top_index = get_first_position(symbol, last_column, top, bottom)
                bottom_index = get_last_position(symbol, last_column, top, bottom)
                top = last_to_first(last_column, top_index)
                bottom = last_to_first(last_column, bottom_index)
            else:
                return 0
        else:
            return bottom - top + 1


def bwt_matching_test(last_column: str = 'TCCTCTATGAGATCCTATTCTATGAAACCTTCA$GACCAAAATTCTCCGGC',
                      patterns_list: List[str] = ['CCT', 'CAC', 'GAG', 'CAG', 'ATC'],
                      answer: List[int] = [2, 1, 1, 0, 1]
                      ):
    print([bwt_matching(last_column, pattern) for pattern in patterns_list])
    assert [bwt_matching(last_column, pattern) for pattern in patterns_list] == answer


def solution(input_file_path: str, output_file_path: str):
    with open(input_file_path, 'r') as file:
        last_column = file.readline().strip()
        patterns_list = (str(file.readline())).split()

    output = [str(bwt_matching(last_column, pattern)) for pattern in patterns_list]

    with open(output_file_path, 'w') as file:
        file.write(' '.join(output))


if __name__ == "__main__":
    bwt_matching_test()
    solution(input_file_path='./input_data/rosalind_ba9l.txt',
             output_file_path='./input_data/rosalind_ba9l_output.txt')
